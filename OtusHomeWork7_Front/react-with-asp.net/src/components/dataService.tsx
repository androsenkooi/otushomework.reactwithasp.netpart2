import axios from 'axios';

class dataService {
    public static serviceURL: string;

    private static client = axios.create({
        baseURL: `https://localhost:7184/`,
        timeout: 200000,
        headers: {
            ContentType: 'application/json',
            Accept: 'application/json',
            'Access-Control-Allow-Credentials': true
        },
    });

    public static async getWeatherForecastData(): Promise<any> {
        const res = await this.client.get(`/WeatherForecast`);
        return res.data;
    }
}

export default dataService;